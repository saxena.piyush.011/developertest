const express=require('express');
const multer=require('multer');
const path=require('path');
const employeeModel=require('../models/employeeModel');
const config=require("../utils/config");
const employeeOperations = require("../db/helpers/employeeOperations");
const imageMimeType=require('../utils/config').imageMimeType;
function logoUpload(req,res) {
    console.log("hello i am here ");
        employeeModel.firstName=req.body.firstName;
        employeeModel.lastName=req.body.lastName;
        employeeModel.status=req.body.status;
        employeeModel.score=req.body.score;
    const storage = multer.diskStorage({
        destination: function (req, file, callBack) {
          error:null,
          callBack(this.error, 'uploads/employeeImage/')
        },
        filename: function (req, file, callBack) {
            // let extArray = file.mimetype.split("/");
            // let extension = extArray[extArray.length - 1];
            callBack(null,file.fieldname + '-' + new Date().toISOString().replace(/:/g, '-') + path.extname(file.originalname));
        }
      });
      const fileFilter = (req, file,cb) => {
        imageMimeType.forEach((imageType) => {
            if(file.mimetype === imageType) {
                cb(null,true);
            }
            else 
            {
                cb(null,false);
            }
        });
      };
      const upload = multer({ storage: storage,limits: {
          fileSize : 1024 * 1024  // 1Mb file size 
      },
      fileFilter : fileFilter  
    }).single('employeeImage')
      upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
            if(err.message==='File too large') {
                res.status(500).json({"message":"File Too Large Upload any other file Please"});
            }
            else {
                res.status(500).json({"message":err});
            }
    } else if (err) {
      res.status(404).json({"message2":err});
    }
    else {
        if(req.file){
        employeeModel.profPic=(req.file.destination + req.file.originalname);
        employeeOperations.uploadEmployeeData(employeeModel,res);
        }
        else{
        employeeModel.profPic=config.defaultImageUrl;
        employeeOperations.uploadEmployeeData(employeeModel,res);
        }
    }
  });
}
module.exports=logoUpload;