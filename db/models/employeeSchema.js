const mongoose = require('../connection');
const employeeSchema = mongoose.Schema;
const employee = new employeeSchema({
    date:{type:String,default:new Date()},
    firstName:{type:String},
    lastName:{type:String},
    profPic:{type:String},
    score:{type:Number},
    status:{type:String,enum:["Active","Inactive"]}
});
const Employee = mongoose.model('employees',employee);
module.exports=Employee;