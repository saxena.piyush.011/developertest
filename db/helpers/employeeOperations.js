const employeeModel=require('../models/employeeSchema');
const config = require('../../utils/config');
const footerOperations = {
    uploadEmployeeData(employeeObject,res) {
        employeeModel.create(employeeObject,(err)=> {
            if(err){
                res.status(500).json({"status":config.ERROR,"message":"Error while uploading the employee Object"});
            }
            else {
                res.status(200).json({"status":config.SUCCESS,"message":"Successfully uploaded employee object "});
            }
        });
    },
    getEmployeesData(res) {
        employeeModel.find({},(err,data)=> {
            if(err) {
                res.status(500).json({"status":config.ERROR,"message":"Error while finding the data of employees "});
            }
            else {
                res.status(200).json({"status":config.SUCCESS,"message":"Successfully find the data","data":data});
            }
        })
    }
}
module.exports=footerOperations;