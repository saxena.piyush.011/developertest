const employeeRouter=require('express').Router();
const multer=require('multer');
const employeeOperations =require("../db/helpers/employeeOperations");
const multerUpload=require("../utils/multer");
const upload = multer({ dest: 'uploads/' });
employeeRouter.post('/profile', upload.single('employeeImage'), function (req, res, next) {
    multerUpload(req,res);
});
employeeRouter.get('/getemployeeData',(req,res)=> {
    employeeOperations.getEmployeesData(res);
});
module.exports=employeeRouter;